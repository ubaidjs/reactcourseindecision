class Dice extends React.Component {
   constructor(props) {
		super(props);
		this.handleRoll = this.handleRoll.bind(this);
		this.state = {
			dice: 0
		}
   }
	
	handleRoll() {
		this.setState(() => {
			return {
				dice: Math.floor(Math.random() * 6 + 1)
			}
		})
	}
	
   render() {
      return (
		  <div>
			  <h1>Number on Dice: {this.state.dice}</h1>
			  <button onClick={this.handleRoll}>Roll the Dice</button>
        </div>
      )
   }
}

ReactDOM.render(<Dice />, document.getElementById('app'));