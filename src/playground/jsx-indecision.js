const app = {
  title: 'Indecision App',
  subtitle: 'This is some subtitle',
  options: []
}

const onFormSubmit = (e) => {
	e.preventDefault();
	const optionText = e.target.elements.options.value;
	if (optionText) {
		app.options.push(optionText);
		e.target.elements.options.value = '';
	}
	renderApp();
}

const removeAllOptions = () => {
	app.options = [];
	renderApp();
}

const makeDecision = () => {
	const randomNum = Math.floor(Math.random() * app.options.length);
	const option = app.options[randomNum];
	console.log(option);
}

const renderApp = () => {
	const template = (
		<div>
			<h1>{app.title}</h1>
			{app.subtitle && <p>{app.subtitle}</p>}
			<button disabled={app.options.length === 0} onClick={makeDecision}>What should I do</button>
			<button onClick={removeAllOptions}>Remove all</button>
			<ol>
				{
					app.options.map((item) => {
						return <li key={app.options.indexOf(item)}>{item}</li>
					})
				}
			</ol>
			<form onSubmit={onFormSubmit}>
				<input type="text" name="options" />
				<button>Add Option</button>
			</form>
		</div>
	);
	ReactDOM.render(template, document.getElementById('app'));
}
renderApp();