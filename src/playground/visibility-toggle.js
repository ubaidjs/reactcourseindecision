class VisToggle extends React.Component {
   constructor(props) {
      super(props);
      this.handleVis = this.handleVis.bind(this);
      this.state = {
         vis: false
      }
   }

   handleVis() {
      this.setState((prevState) => {
         return {
            vis: !prevState.vis
         }
      }) 
      
   }

   render() {
      return (
         <div>
            <button onClick={this.handleVis}>{this.state.vis ? 'Hide Details' : 'Show Details'}</button>
            {
               this.state.vis && (
                  <div>This is the details we were talking about</div>
               )
            }
         </div>
      );
   }
}

ReactDOM.render(<VisToggle />, document.getElementById('app'));