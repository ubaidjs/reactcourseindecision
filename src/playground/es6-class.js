class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    getAge() {
        console.log(`${this.age} is ${this.age} years old.`);
    }
    getDescription() {
        return `${this.age} is ${this.age} years old.`
    }
}

const newPersonInstance = new Person('Ubaid Sid', 20);
console.log(newPersonInstance);

class Student extends Person {
    constructor(major) {
        super(name, age);
        this.major = major;
    }
    getMajor() {
        console.log(`${this.name} has major in ${this.major}`);
    }
    getDescription() {
        let description = super.getDescription();
        console.log(`${description} ${this.name} has major in ${this.major}`)
    }
}

const newStudentInstance = new Student('Someone Else', 21, 'Computer Science');
console.log(newStudentInstance)



