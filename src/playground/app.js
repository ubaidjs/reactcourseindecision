class IndecisionApp extends React.Component {
   constructor(props) {
      super(props);
      this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
      this.handlePick = this.handlePick.bind(this);
      this.handleAddOptions = this.handleAddOptions.bind(this);
      this.state = {
         options: []
      }
   }

   handleDeleteOptions() {
      this.setState(() => {
         return {
            options: []
         }
      });
   }

   handlePick() {
      const randomNum = Math.floor(Math.random() * this.state.options.length);
      console.log(this.state.options[randomNum]);
   }

   handleAddOptions(inputOption) {

      if (!inputOption) {
         return 'Please enter a valid input';
      } else if (this.state.options.indexOf(inputOption) > -1) {
         return 'Oops! This option already exists'
      }

      this.setState((prevState) => {
         return {
            options: prevState.options.concat(inputOption)
         }
      })
   }

   render() {
      const subtitle = 'Choice made by computer';

      return (
         <div>
            <Header subtitle={subtitle}/>
            <Action hasOptions={this.state.options.length > 0} handlePick={this.handlePick}/>
            <Options 
               options={this.state.options} 
               handleDeleteOptions={this.handleDeleteOptions}
            />
            <AddOptions handleAddOptions={this.handleAddOptions}/>
         </div>
      );
   }
}
// Header converted to stateless functional component from class based component
const Header = (props) => {
   return (
      <div>
         <h1>{props.title}</h1>
         <p>{props.subtitle}</p>
      </div>
   );
}

Header.defaultProps = {
   title: 'Indecision App'
}

// class Header extends React.Component {
//    render() {
//       return (
//          <div>
//             <h1>{this.props.title}</h1>
//             <p>{this.props.subtitle}</p>
//          </div>
//       );
//    }
// }

// Action converted to stateless functional component from class based component
const Action = (props) => {
   return (
      <div>
         <button disabled={!props.hasOptions} onClick={props.handlePick}>
            What should I do?
         </button>
      </div>
   );
}

// class Action extends React.Component {
//    render() {
//       return (
//          <div>
//             <button disabled={!this.props.hasOptions} onClick={this.props.handlePick}>
//                What should I do?
//             </button>
//          </div>
//       );
//    }
// }

// Options converted to stateless functional component from class based component
const Options = (props) => {
   return (
      <div>
         <button onClick={props.handleDeleteOptions}>Remove All</button>
         {props.options.map((item) => {
            return <SingleOption key={item} optionText={item} />
         })}
      </div>
   );
}

// class Options extends React.Component {
//    render() {
//       return (
//          <div>
//             <button onClick={this.props.handleDeleteOptions}>Remove All</button>
//             {this.props.options.map((item) => {
//                return <SingleOption key={item} optionText={item} />
//             })}
//          </div>
//       );
//    }
// }

// SingleOption converted to stateless functional component from class based component
const SingleOption = (props) => {
   return (
      <p>
         {props.optionText}
      </p>
   );
}

// class SingleOption extends React.Component {
//    render() {
//       return (
//          <p>
//             {this.props.optionText}
//          </p>
//       );
//    }
// }

class AddOptions extends React.Component {
   constructor(props) {
      super(props)
      this.handleAddOptionsLocal = this.handleAddOptionsLocal.bind(this);
      this.state = {
         error: undefined
      }
   }
   handleAddOptionsLocal(e) {
      e.preventDefault();
  
      const option = e.target.elements.option.value.trim();
      const error = this.props.handleAddOptions(option);

      this.setState(() => {
         return {
            error
         }
      })

      e.target.elements.option.value = '';
   }
   render() {
      return (
         <div>
            {this.state.error && <p>{this.state.error}</p>}
            <form onSubmit={this.handleAddOptionsLocal}>
               <input type="text" name="option" />
               <button>Add Option</button>
            </form>
         </div>
      );
   }
}

ReactDOM.render(<IndecisionApp />, document.getElementById('app'));