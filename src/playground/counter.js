class Counter extends React.Component {
   constructor(props) {
      super(props)
      this.handleAddOne = this.handleAddOne.bind(this);
      this.handleMinus = this.handleMinus.bind(this);
      this.handleReset = this.handleReset.bind(this);
      this.state = {
         count: props.count
      }
   }

   componentDidMount() {
      try {
         const json = localStorage.getItem('count');
         const count = JSON.parse(json);
   
         if (count) {
           this.setState(() => ({ count }));
         }
       } catch (e) {
         // Do nothing at all
       }
   }

   componentDidUpdate (prevProps, prevState) {
      const json = JSON.stringify(this.state.count);
      localStorage.setItem('count', json);
   }

   handleAddOne() {
      this.setState((prevState) => {
         return {
            count: prevState.count + 1
         };
      });
   }

   handleMinus() {
      this.setState((prevState) => {
         return {
            count: prevState.count - 1
         }
      });
   }

   handleReset() {
      this.setState(() => {
         return {
            count: 0
         }
      });  
   }

   render() {
      return (
         <div>
            <h1>Count: {this.state.count}</h1>
            <button onClick={this.handleAddOne}>+1</button>
            <button onClick={this.handleMinus}>-1</button>
            <button onClick={this.handleReset}>reset</button>
         </div>
      );
   }
}

Counter.defaultProp = {
   count: 0
};

ReactDOM.render(<Counter count={0} />, document.getElementById('app'));